import express from "express";

const app = express()

app.get("/", (req, res) => {
    console.log(`Request from ${getClientAddress(req)}`);
    res.json({
        msg: "Hello, World. Its an Express Server 😎"
    })
});

app.listen(3000, () => {
    console.log("Listening on port 3000.")
});

function getClientAddress(req) {
    return req.headers['x-forwarded-for'] || req.connection.remoteAddress;
}